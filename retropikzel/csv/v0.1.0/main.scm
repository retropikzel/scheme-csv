(define-library
  (retropikzel csv v0.1.0 main)
  (import (scheme base)
          (scheme write)
          (scheme file)
          (retropikzel string-util v0.8.0 main))
  (export csv->list
          csv-parse-file
          csv-from-list)

  (begin

    (define slurp
      (lambda (file-name)
        (letrec ((read-all (lambda (result line)
                             (if (eof-object? line)
                               result
                               (read-all (cons line result)
                                         (read-string 4000))))))
          (with-input-from-file
            file-name
            (lambda ()
              (apply string-append
                     (reverse (read-all (list) (read-string 4000)))))))))

    (define csv->list
      (lambda (csv-text delimiter)
        (map (lambda (line)
               (string-util-split-by-char-if-not-inside line delimiter #\"))
             (string-util-split-by-char csv-text #\newline))))1

    (define csv-parse-file
      (lambda (path delimiter)
        (csv->list (slurp path) delimiter)))

    (define csv-from-list
      (lambda (csv-list delimiter)
        (apply
          string-append
          (map (lambda (line)
                 (string-append (string-util-join line (string delimiter))
                                (string #\newline)))
               csv-list))))))
