(import (scheme base)
        (scheme write)
        (scheme file)
        (scheme process-context)
        (retropikzel csv v0.1.0 main))

(define path (list-ref (command-line) 1))

(define csv-list (csv-parse-file path #\,))
(with-output-to-file
  (string-append "tmp/" path)
  (lambda ()
    (display (csv-from-list csv-list #\,))))
