#!/usr/env bash

set -euo pipefail

mkdir -p tmp/test-data


echo "" > test-data/generated-customers-1000.csv
for i in $(seq 1 10)
do
    cat test-data/customers-100.csv >> test-data/generated-customers-1000.csv
done

for file in ./test-data/*
do
    echo "Testing $file"
    bash test.sh "$file"
    diff "$file" "tmp/$file" > /dev/null || echo "Files $file and tmp/$file are not identical"
done

