(define-library
  (retropikzel string-util v0.8.0 main)
  (import (scheme base)
          (scheme write)
          (scheme char))
  (export string-util-split-by-char
          string-util-split-by-char-if-not-inside
          string-util-split-by-string
          string-util-join
          string-util-reverse
          string-util-count-char
          string-util-contains-char?
          string-util-strip-prefix
          string-util-strip-suffix
          string-util-in-list?
          string-util-starts-with?
          string-util-starts-with-any?
          string-util-ends-with?
          string-util-append!
          string-util-display->string
          string-util-replace-all
          string-util-replace-char
          string-util-replace-char-if-not-inside
          string-util-replace-count
          string-util-replace-with-tags)
  (begin

    (define string-util-contains-char?
      (lambda (str mark)
        (cond
          ((= (string-length str) 0) #f)
          ((char=? mark (string-ref str 0)) #t)
          ((= (string-length str) 1) #f)
          (else (string-util-contains-char? (string-copy str 1) mark)))))

    (define string-util-split-by-char
      (lambda (str mark)
        (let* ((str-l (string->list str))
               (res (list))
               (last-index 0)
               (index 0)
               (splitter (lambda (c)
                           (cond ((char=? c mark)
                                  (begin
                                    (set! res (append res (list (substring str last-index index))))
                                    (set! last-index (+ index 1))))
                                 ((equal? (length str-l)  (+ index 1))
                                  (set! res (append res (list (substring str last-index (+ index 1)))))))
                           (set! index (+ index 1)))))
          (for-each splitter str-l)
          res)))

    (define string-util-split-by-char-if-not-inside
      (lambda (str mark inside)
        (let* ((str-l (string->list str))
               (res (list))
               (last-index 0)
               (index 0)
               (inside? #f)
               (splitter (lambda (c)
                           (cond ((and (char=? c inside)
                                       (not inside?))
                                  (set! inside? #t))
                                 ((and (char=? c inside)
                                       (not inside?))
                                  (set! inside? #t)))
                           (cond ((and (not inside?)
                                       (char=? c mark))
                                  (begin
                                    (set! res (append res (list (substring str last-index index))))
                                    (set! last-index (+ index 1))))
                                 ((equal? (length str-l)  (+ index 1))
                                  (set! res (append res (list (substring str last-index (+ index 1)))))))
                           (set! index (+ index 1)))))
          (for-each splitter str-l)
          res)))

    (define string-util-split-by-string
      (lambda (str mark)
        (if (string=? mark "")
          str
          (letrec* ((mark-length (string-length mark))
                    (str-length (string-length str))
                    (index-end (- str-length mark-length))
                    (looper
                      (lambda (result index last-index)
                        (cond
                          ((= index str-length)
                           (append result
                                   (list (string-copy str last-index str-length))))
                          ((and (<= index index-end)
                                (string=? (string-copy str index (+ index mark-length)) mark))
                           (looper (append result (list (string-copy str last-index index)))
                                   (+ index mark-length)
                                   (+ index mark-length)))
                          (else (looper result (+ index 1) last-index))
                          ))))
            (looper (list) 0 0)))))

    (define string-util-join
      (lambda (l between)
        (letrec ((looper (lambda (l result between)
                           (if (null? l)
                             result
                             (looper (cdr l)
                                     (if (and (>= (length l) 1)
                                              (not (string=? result "")))
                                       (string-append result between (car l))
                                       (string-append result (car l)))
                                     between)))))
          (looper l "" between))))

    (define string-util-reverse
      (lambda (str)
        (list->string (reverse (string->list str)))))

    (define string-util-starts-with?
      (lambda (str prefix)
        (and (>= (string-length str) (string-length prefix))
             (string=? (string-copy str 0 (string-length prefix)) prefix))))

    (define string-util-starts-with-any?
      (lambda (str prefixes)
        (cond ((null? prefixes) #f)
              ((string-util-starts-with? str (car prefixes)) #t)
              (else (string-util-starts-with-any? str (cdr prefixes))))))

    (define string-util-ends-with?
      (lambda (str suffix)
        (and (>= (string-length str) (string-length suffix))
             (string=? (string-copy str (- (string-length str)
                                           (string-length suffix)))
                       suffix))))

    (define string-util-count-char
      (lambda (str char-to-count)
        (let ((count 0))
          (string-for-each
            (lambda (char-in-string)
              (if (char=? char-in-string char-to-count)
                (set! count (+ count 1))))
            str)
          count)))

    (define string-util-strip-prefix
      (lambda (str chars)
        (cond ((= (string-length str) 0) str)
              ((string-util-contains-char? chars (string-ref str 0))
               (string-util-strip-prefix (string-copy str 1) chars))
              (else str))))

    (define string-util-strip-suffix
      (lambda (str chars)
        (string-util-reverse (string-util-strip-prefix (string-util-reverse str)
                                                       chars))))

    (define string-util-strip-suffix-old
      (lambda (str chars)
        (let ((cut-point 0))
          (string-for-each
            (lambda (c)
              (if (string-util-contains-char? chars c)
                (set! cut-point (+ cut-point 1))))
            (string-util-reverse str))
          (if (= cut-point 0)
            str
            (string-copy str 0 (- (string-length str) cut-point))))))

    (define string-util-strip
      (lambda (str chars)
        (string-util-strip-suffix (string-util-strip-prefix str chars) chars)))

    (define string-util-in-list?
      (lambda (str l)
        (cond ((null? l) #f)
              ((and (string? (car l)) (string=? (car l) str)) #t)
              (else (string-util-in-list? str (cdr l))))))

    (define-syntax string-util-append!
      (syntax-rules ()
        ((_ string-1 string-2)
         (set! string-1 (string-append string-1 string-2)))))

    (define string-util-display->string
      (lambda (to-display)
        (parameterize
          ((current-output-port
             (open-output-string)))
          (display to-display)
          (get-output-string (current-output-port)))))

    (define string-util-replace-all
      (lambda (text replace replace-with)
        (letrec ((looper (lambda (result text)
                           (if (or (string=? replace "")
                                   (string=? text "")
                                   (= (string-length text) 0))
                             result
                             (if (and (char=? (string-ref replace 0) (string-ref text 0))
                                      (>= (string-length text) (string-length replace))
                                      (string=? (string-copy text 0 (string-length replace))
                                                replace))
                               (looper (string-append result replace-with)
                                       (string-copy text (string-length replace)))
                               (looper (string-append result (string-copy text 0 1))
                                       (string-copy text 1)))))))
          (looper "" text))))

    (define string-util-replace-char
      (lambda (text replace replace-with)
        (string-map
          (lambda (c)
            (if (char=? c replace)
              replace-with
              c))
          text)))

    (define string-util-replace-char-if-not-inside
      (lambda (text replace replace-with inside)
        (let ((inside? #f))
          (string-map
            (lambda (c)
              (cond ((and (char=? c inside)
                          (not inside?))
                     (set! inside? #t))
                    ((and (char=? c inside)
                          (not inside?))
                     (set! inside? #t)))
              (if (and (not inside?)
                       (char=? c replace))
                replace-with
                c))
            text))))

    (define string-util-replace-count
      (lambda (text replace replace-with count)
        (letrec* ((inner-count 0)
                  (looper (lambda (result text)
                            (if (or (string=? replace "")
                                    (string=? text "")
                                    (= (string-length text) 0)
                                    (= inner-count count))
                              (string-append result text)
                              (if (and (char=? (string-ref replace 0) (string-ref text 0))
                                       (>= (string-length text) (string-length replace))
                                       (string=? (string-copy text 0 (string-length replace))
                                                 replace))
                                (begin
                                  (set! inner-count (+ inner-count 1))
                                  (looper (string-append result replace-with)
                                          (string-copy text (string-length replace))))
                                (looper (string-append result (string-copy text 0 1))
                                        (string-copy text 1)))))))
          (looper "" text))))

    (define string-util-replace-with-tags
      (lambda (text replace open-tag close-tag)
        (letrec* ((open? #f)
                  (looper (lambda (result text)
                            (if (or (string=? replace "")
                                    (string=? text "")
                                    (= (string-length text) 0))
                              result
                              (if (and (char=? (string-ref replace 0) (string-ref text 0))
                                       (>= (string-length text) (string-length replace))
                                       (string=? (string-copy text 0 (string-length replace))
                                                 replace))
                                (looper (string-append result
                                                       (if open?
                                                         (begin
                                                           (set! open? (not open?))
                                                           close-tag)
                                                         (begin
                                                           (set! open? (not open?))
                                                           open-tag)))
                                        (string-copy text (string-length replace)))
                                (looper (string-append result (string-copy text 0 1))
                                        (string-copy text 1)))))))
          (looper "" text))))


    ))
